use std::env;
use std::f64::consts;
use std::collections::HashMap;

fn main() {
    let mut stack = Vec::new();
    let mut result;

    //binary operations
    let mut operations: HashMap<&str, fn(f64, f64) -> f64> = HashMap::new();
        //arithmetic
    operations.insert("+",      |x, y| x + y);
    operations.insert("x",      |x, y| x * y);
    operations.insert("-",      |x, y| y - x);   //due to how stacks work, it is easier to just reverse operands
    operations.insert("/",      |x, y| y / x);   //for operations where the order actually matters
    operations.insert("\\",     |x, y| x / y);
    operations.insert("^",      |x, y| y.powf(x));
        //calculus
    operations.insert("log",    |x, y| y.log(x));
        //maximum and minimum
    operations.insert("max",    |x, y| x.max(y));
    operations.insert("min",    |x, y| x.min(y));

    //functions and unary operations
    let mut functions: HashMap<&str, fn(f64) -> f64> = HashMap::new();
        //arithmetic
    functions.insert("abs",     |x| x.abs());
    functions.insert("cbrt",    |x| x.cbrt());
    functions.insert("sign",    |x| x.signum());
    functions.insert("sqrt",    |x| x.sqrt());
        //calculus
    functions.insert("exp",     |x| x.exp());
    functions.insert("exp2",    |x| x.exp2());
    functions.insert("ln",      |x| x.ln());
    functions.insert("log2",    |x| x.log2());
    functions.insert("log10",   |x| x.log10());
        //rounding
    functions.insert("ceil",    |x| x.ceil());
    functions.insert("floor",   |x| x.floor());
    functions.insert("round",   |x| x.round());
        //trigonometry
    functions.insert("acos",    |x| x.acos());
    functions.insert("asin",    |x| x.asin());
    functions.insert("atan",    |x| x.atan());
    functions.insert("acosh",   |x| x.acosh());
    functions.insert("asinh",   |x| x.asinh());
    functions.insert("atanh",   |x| x.atanh());
    functions.insert("cos",     |x| x.cos());
    functions.insert("sin",     |x| x.sin());
    functions.insert("tan",     |x| x.tan());
    functions.insert("cosh",    |x| x.cosh());
    functions.insert("sinh",    |x| x.sinh());
    functions.insert("tanh",    |x| x.tanh());

    //constants
    let mut constants: HashMap<&str, f64> = HashMap::new();
    constants.insert("e",       consts::E);
    constants.insert("pi",      consts::PI);
    constants.insert("tau",     consts::TAU);
    constants.insert("sqrt_2",  consts::SQRT_2);

    for arg in env::args() {
        match arg.parse::<f64>() {
            Ok(num) => stack.push(num),
            Err(_) => {
                if arg.contains("rpn") {
                } else if operations.contains_key(arg.as_str()) {
                    result = operations[arg.as_str()](
                        stack.pop().unwrap(),
                        stack.pop().unwrap_or_else(||panic!("Provide more arguments"))
                        );
                    stack.push(result)
                } else if functions.contains_key(arg.as_str()) {
                    result = functions[arg.as_str()](
                        stack.pop().unwrap_or_else(||panic!("Provide more arguments"))
                        );
                    stack.push(result)
                } else if constants.contains_key(arg.as_str()) {
                    result = constants[arg.as_str()];
                    stack.push(result)
                } 
            },
        }
    }

    if stack.len() < 1 {
        panic!("Provide some arguments");
    } else if stack.len() > 1 {
        panic!("Too many arguments");
    }

    println!("{}", stack.pop().unwrap());
}
